### 为HC32F448的使用VSCODE开发项目

    大部分源码来自<HC32F448_DDL_Rev1.0.0>项目

### 我只负责编写
    link.cfg
        编程器部分,openocd切换调色器版本
    hc32f448.cfg
        编程器部分,openocd处理目标MCU脚本
    hc32f448_flash.tcl
        编程器部分,openocd处理HC32F448的flash编程与擦除(未完善,只处理基本.)
    write_flash.tcl
        编程器部分,一键烧录
    ihex2tcl.lua
        数据转换, hex文件转烧录文件

### 需要软件
    lua5.4
    cmake
    ninja
    vscode
    arm-none-eabi-gcc 工具链

### 构建项目
    mkdir build
    cd build
    cmake ../ -DCMAKE_BUILD_TYPE=debug -DCMAKE_TOOLCHAIN_FILE=../cortex_m.cmake

## 烧录目标板子
#### 连接daplink仿真器
    openocd -f link.cfg -f hc32f448.cfg -f hc32f448_flash.tcl -f write_flash.tcl
#### 另外打开命令行
    telnet localhost 4444
*   进入telnet模式后使用以下命令

    >write-flash

