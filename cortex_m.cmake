set(CMAKE_SYSTEM_NAME Generic)

# which compilers to use for C and C++
set(CMAKE_C_COMPILER   arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER arm-none-eabi-g++)

set(LINKER_SCRIPT "${CMAKE_CURRENT_LIST_DIR}/linker/HC32F448xC.ld")
set(MAP_FILE    "main.map")

set(CMAKE_C_FLAGS "-mlong-calls -mcpu=cortex-m4 -mthumb -ffunction-sections -mfloat-abi=hard -mfpu=fpv4-sp-d16 -ffunction-sections")
set(CMAKE_EXE_LINKER_FLAGS "-mcpu=cortex-m4 -mthumb  -ffunction-sections -T ${LINKER_SCRIPT} -Xlinker --gc-sections -Wl,-Map,${MAP_FILE}  --specs=nano.specs --specs=nosys.specs")
set(CMAKE_EXE_LINKER_FLAGS_INIT "-mcpu=cortex-m4 -mthumb  -ffunction-sections -Xlinker --gc-sections --specs=nano.specs --specs=nosys.specs")

# where is the target environment located
# set(CMAKE_FIND_ROOT_PATH  /usr/i586-mingw32msvc
#     /home/alex/mingw-install)

# adjust the default behavior of the FIND_XXX() commands:
# search programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# search headers and libraries in the target environment
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)