

proc write-flash { } {
    reset halt;
    xflash-unlock
    xflash-erase-all
    xflash-program-start
    echo "start program"
    source build/main_elf.tcl
    xflash-program-end
}
